package test.lingpipe;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import com.aliasi.classify.Classification;
import com.aliasi.classify.Classified;
import com.aliasi.classify.DynamicLMClassifier;
import com.aliasi.classify.JointClassification;
import com.aliasi.classify.JointClassifier;
import com.aliasi.util.AbstractExternalizable;

import test.itext.TestiText;

public class TestLingPipe {
	String dataFolder = "/Users/vadidam/usr/private/doc_categorization/data";
	String testFile = "/Users/vadidam/usr/private/doc_categorization/testFiles/testFile_1.pdf";
	String destFolder = "/Users/vadidam/usr/private/doc_categorization/destFolder";
	
	private static int NGRAM_SIZE = 6;

	String[] getCategories(String folderName) throws IOException {
		ArrayList<String> folders = new ArrayList<String>();

		File dir = new File(folderName);
		if (dir.canRead() == false) {
			throw new IOException("Cannot read Folder:" + folderName);
		}
		if (dir.isDirectory() == false) {
			throw new IOException("Is not a Folder:" + folderName);
		}

		File[] contents = dir.listFiles();
		for (File file : contents) {
			if (file.isDirectory()) {
				folders.add(file.getName());
				//System.out.println(file.getName());
			}
		}

		return folders.toArray(new String[0]);

	}

	public String getTextFromPDF(File pdfFile) throws IOException {
		InputStream is = null;

		try {
			is = new FileInputStream(pdfFile);
			return TestiText.extractTextFromPDF(is);

		} finally {
			if (is != null)
				is.close();
		}
	}

	public void trainClassifier(DynamicLMClassifier<?> trainClassifier) throws IOException {
		String categories[] = getCategories(dataFolder);

		for (String category : categories) {
			Classification classification = new Classification(category);

			File categoryFolder = new File(dataFolder, category);
			FilenameFilter filter = new FilenameFilter() {

				public boolean accept(File dir, String name) {
					return name.toLowerCase().endsWith(".pdf");
				}
				
			};
			File trainingFiles[] = categoryFolder.listFiles(filter);
			for (File trainingFile : trainingFiles) {
				String text=getTextFromPDF(trainingFile);
				Classified<CharSequence> classified = new Classified<CharSequence>(text, classification);
				trainClassifier.handle(classified);
			}

		}

	}
	
	public String getBestCategory(JointClassifier<CharSequence> compiledClassifier, String text) {
		JointClassification jc = compiledClassifier.classify(text);
		String bestCategory = jc.bestCategory();
		
		return bestCategory;
		
	}

	public static void main(String args[]) {
		TestLingPipe tlp = new TestLingPipe();

		try {
			
			//Setup Classifier
			String categories[] = tlp.getCategories(tlp.dataFolder);
			DynamicLMClassifier<?> classifier = DynamicLMClassifier.createNGramBoundary(categories, tlp.NGRAM_SIZE);

			//Train
			tlp.trainClassifier(classifier);
			
			//Classify by Page
			JointClassifier<CharSequence> compiledClassifier
		    = (JointClassifier<CharSequence>)
		      AbstractExternalizable.compile(classifier);
			String[] pages = TestiText.extractTextFromPDFByPage(new FileInputStream(tlp.testFile));
			String[] suggestedCategories = new String[pages.length];
			for (int i=0; i<pages.length;i++) {
				suggestedCategories[i] = tlp.getBestCategory(compiledClassifier, pages[i]);
			}
			
			TestiText.writePagesToCategoryFolders(new FileInputStream(tlp.testFile), suggestedCategories, new File(tlp.destFolder));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
