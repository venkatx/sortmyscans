package test.itext;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;

public class MergePDF {
	
	public PdfPage[] getPagesFromPDFFile(File pdfFile) throws IOException {
		PdfPage ret[] = null;
		
		FileInputStream src = null;
		
		try {
			
			src = new FileInputStream(pdfFile);
			System.out.println(pdfFile.getAbsolutePath());
			
			PdfReader reader = new PdfReader(src);
		    PdfDocument origPdf = new PdfDocument(reader);
			
		    int numberOfPages = origPdf.getNumberOfPages();
		    ret = new PdfPage[numberOfPages];
		    for(int i=0; i<numberOfPages; i++) {
		    	ret[i] = origPdf.getPage(i+1);
		    }
		    
			return ret;
		} finally {
			if (src != null) src.close();
		}
	}
	
	public void convertFolderToPDF(File folder, File destPdf) throws IOException {
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				return name.toLowerCase().endsWith(".pdf");
			}
			
		};
		
		FileOutputStream fos = new FileOutputStream(destPdf);
		PdfWriter writer = new PdfWriter(fos);
		PdfDocument outputDoc = new PdfDocument(writer);
		
		
		
		File files[] = folder.listFiles(filter);
		
		for (File file:files) {
			PdfPage[] pages = getPagesFromPDFFile(file);
			for(PdfPage page:pages) {
				outputDoc.addPage(page.copyTo(outputDoc));
			}
			
		}
		
		outputDoc.close();
		fos.close();
	}
	
	public void convertFoldersToPDF(File inputFolder, File destFolder) throws IOException {
		File[] files = inputFolder.listFiles();
		for (File file:files) {
			if (file.isDirectory() == true) {
				File destPdf = new File(destFolder, file.getName() + ".pdf");
				convertFolderToPDF(file, destPdf);
			}
		}
	}
	
	public static void main(String args[]) {
		File srcFolder = new File("/Users/vadidam/usr/private/doc_categorization/destFolder");
		File destFolder = new File("/Users/vadidam/usr/private/doc_categorization/mergedPDFs");
		
		MergePDF mp = new MergePDF();
		try {
			mp.convertFoldersToPDF(srcFolder, destFolder);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
