package test.itext;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfPage;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.parser.PdfTextExtractor;
import com.itextpdf.layout.Document;

public class TestiText {
	
	public static String extractText(PdfPage page) throws IOException {
		return PdfTextExtractor.getTextFromPage(page);
	}
	
	public static String extractTextFromPDF(InputStream pdfFile) throws IOException {
		StringBuffer ret = new StringBuffer();
		
		PdfReader reader = new PdfReader(pdfFile);
		PdfDocument origPdf = new PdfDocument(reader);
		
		int numberOfPages = origPdf.getNumberOfPages();
	    for (int i=0; i<numberOfPages; i++) {
	    	PdfPage page = origPdf.getPage(i+1);
	    	ret.append(extractText(page));
	    }
		
	    return ret.toString();
	}
	
	public static String[] extractTextFromPDFByPage(InputStream pdfFile) throws IOException {
		ArrayList<String> ret = new ArrayList<String>();
		
		PdfReader reader = new PdfReader(pdfFile);
		PdfDocument origPdf = new PdfDocument(reader);
	    int numberOfPages = origPdf.getNumberOfPages();
	    for (int i=0; i<numberOfPages; i++) {
	    	PdfPage page = origPdf.getPage(i+1);
	    	ret.add(extractText(page));
	    }
	    
	    return ret.toArray(new String[0]);
	}
	
	private static void extractPageToFile(File destDir, String category, PdfPage page) throws IOException {
		int currentLength = destDir.listFiles().length;
		
		File destFile = new File(destDir, category+"_"+currentLength+".pdf");
		FileOutputStream fos = new FileOutputStream(destFile);
		PdfWriter writer = new PdfWriter(fos);
		PdfDocument outputDoc = new PdfDocument(writer);
		outputDoc.addPage(page.copyTo(outputDoc));
		outputDoc.close();
		fos.close();
	}
	
	public static File createNewCategoryFolder(File outputFolder, String category) {
		int currentNumberOfFolders = outputFolder.listFiles().length;
		
		File newFolder = new File(outputFolder, category+"_" +currentNumberOfFolders);
		newFolder.mkdirs();
		
		return newFolder;
	}
	
	public static void writePagesToCategoryFolders(InputStream pdfFile, String[] categories, File outputFolder) throws IOException {
		PdfReader reader = new PdfReader(pdfFile);
	    PdfDocument origPdf = new PdfDocument(reader);
	    
	    int numberOfPages = origPdf.getNumberOfPages();
	    String currentCategory = "";
	    File currentCategoryFolder = null;
	    for (int i=0; i<numberOfPages; i++) {
	    	PdfPage page = origPdf.getPage(i+1);
	    	if (currentCategory.equalsIgnoreCase(categories[i]) == false) 
	    	{
	    		currentCategory = categories[i];
	    		currentCategoryFolder = createNewCategoryFolder(outputFolder,currentCategory);
	    	}
	    	
	    	extractPageToFile(currentCategoryFolder,currentCategory,page);
	    }
	}
	
	public static void main(String[] args) throws IOException {
		String src = "/Users/vadidam/delete/test.pdf";
		File destDir = new File("/Users/vadidam/delete/testExtraction");
		if (destDir.exists() == false) destDir.mkdirs();
		
	    PdfReader reader = new PdfReader(src);
	    PdfDocument origPdf = new PdfDocument(reader);
	    int numberOfPages = origPdf.getNumberOfPages();
	    for (int i=0; i<numberOfPages; i++) {
	    	PdfPage page = origPdf.getPage(i+1);
	    	
	    	//extractPageToFile(destDir, i, page);
	    	
	    	FileUtils.writeStringToFile(new File(destDir,i+".txt"), extractText(page));
	    }
	    
	    origPdf.close();
	    
	    
	}

	
	
}
